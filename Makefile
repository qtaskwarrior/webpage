VENV = VENV
VENV_ACTIVATE = . $(VENV)/bin/activate
NIKOLA = $(VENV_ACTIVATE); nikola

.PHONY = deploy-osdn deploy-sf build all po4a clean auto pip-install-requirements requirements.lock.txt venv

build: po4a
	$(NIKOLA) build

auto: po4a
	$(NIKOLA) auto

po4a:
	po4a po4a.cfg

deploy-osdn: build
	$(NIKOLA) deploy osdn

deploy-sf: build
	$(NIKOLA) deploy sourceforge

pip-install-requirements:
	$(VENV_ACTIVATE); pip install -r requirements.txt

requirements.lock.txt:
	$(VENV_ACTIVATE); pip freeze > $@

venv:
	virtualenv VENV

clean:
	rm -rf pages/*.*.rst cache output
