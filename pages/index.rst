.. title: Home
============
qtaskwarrior
============

:Author: Jaime Marquínez Ferrándiz
:License: `Unlicense`_


`qtaskwarrior` is a graphical interface for the `taskwarrior <https://taskwarrior.org/>`_ program using the Qt framework.

.. contents::

Get it
------

The program can be installed from `Itch`_:

.. raw:: html

   <iframe src="https://itch.io/embed/987894" width="552" height="167" frameborder="0"><a href="https://jaimemf.itch.io/qtaskwarrior">qtaskwarrior by jaimeMF</a></iframe>

There are also repositories for different Linux distributions at `Open Build Service`_. There you can also download the `AppImage`_.

Screenshots
-----------

.. thumbnail:: /images/task_list.png
   :scale: 50%
   :alt: The task list

   The main window contains the list of tasks.

.. thumbnail:: /images/task_edit.png
   :scale: 50%
   :alt: The task edit window

   The window for editing a task.


Developer information
---------------------

The project and this page is hosted on `OSDN`_.

.. image:: https://osdn.net/sflogo.php?group_id=12990&type=3
   :alt: OSDN
   :target: `OSDN`_
   :width: 210
   :height: 63

You can see information from the project at `OpenHub`_:

.. From https://www.openhub.net/p/qtaskwarrior/widgets/project_factoids?format=js

.. raw:: html

   <iframe src='https://www.openhub.net/p/qtaskwarrior/widgets/project_factoids' scrolling='no' marginHeight='0' marginWidth='0' style='height: 175px; width: 350px; border: none'></iframe>

Support
-------

If you find any problem you can report it at the `OSDN tickets <https://osdn.net/projects/qtaskwarrior/ticket/>`_.


.. _Unlicense: https://unlicense.org/

.. _OSDN: https://osdn.net/projects/qtaskwarrior/

.. _Itch: https://jaimemf.itch.io/qtaskwarrior

.. _AppImage: https://download.opensuse.org/repositories/home:/jaimeMF:/qtaskwarrior/AppImage/qtaskwarrior-latest-x86_64.AppImage

.. _Open Build Service: https://software.opensuse.org//download.html?project=home%3AjaimeMF%3Aqtaskwarrior&package=qtaskwarrior

.. _OpenHub: https://www.openhub.net/p/qtaskwarrior
